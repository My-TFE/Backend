'use strict'

/**
 * Customers Routes
 */

const routes = require('express').Router()
const { getList, createItem, updateItem, getItem, deleteItem } = require('./handler')

// TODO: Add middleware(s) here for a fine-grained control over specific routes
// routes.all("*", onlyAdmin);
// routes.post("*", onlyAdmin);

routes.get('/', getList)
routes.post('/', createItem)
routes.put('/:id', updateItem)
routes.get('/:id', getItem)
routes.delete('/:id', deleteItem)

module.exports = { routes }
