'use strict'

const jwt = require('jsonwebtoken')
const logger = require('../../logger')
const { i18n } = require('../../config')
const Coach = require('../../models/Coach')
const destroyToken = require('../../middlewares/jwt-middleware')
const secret = new Buffer(process.env.JWT_SUPER_SECRET, 'base64')

const login = async (req, res) => {
  const { email, password } = req.body
  try {
    const coach = await Coach.findOne({ email: email })
      .select('firstName lastName password')
      .exec()
    if (coach) {
      if (!coach.authenticate(password)) {
        res.apiResponse({
          success: false,
          status: 200,
          message: i18n.__('Authentication failed')
        })
      } else {
        destroyToken.revokeToken(coach._id)
        const jwtPayload = {
          id: coach._id,
          displayName: `${coach.firstName}${coach.lastName}`
        }
        const token = jwt.sign(jwtPayload, secret, {expiresIn: '30d'})     
        res.apiResponse({
          success: true,
          status: 200,
          data: token
        })
      }
    } else {
      logger.error(`Unknown coach : ${email}`)
      res.apiUnauthorized(new Error(i18n.__('Coach authentication failed')))
    }
  } catch (error) {
    logger.error(error)
    res.apiNotFound(new Error(i18n.__('No coach found')))
  }
}

const getMe = async (req, res) => {
  let token = req.headers.authorization
  if (token) {
    token = token.substr(7)
    //token.replace('Bearer ', '')
    if (token !== 'null') {
      const user = jwt.decode(token, secret)
      if (!user) {
        return res.apiResponse({
          success: false,
          status: 401,
          message: i18n.__('Invalid Token')
        })
      }

      try {
        const coach = await Coach.findOne({ _id: user.id })
        if (coach) {
          res.apiResponse({
            success: true,
            status: 200,
            data: coach
          })
        } else {
          res.apiNotFound(new Error(i18n.__('No coach found')))
        }

      } catch (error) {
        logger.error(error)
        res.apiNotFound(new Error(i18n.__('No coach found')))
      }
    } else {
      res.apiResponse({
        success: false,
        status: 200,
        data: null
      })
    } 
  } else {
    res.apiResponse({
      success: false,
      status: 200,
      message: i18n.__('No token found')
    })
  }
}

module.exports = { login, getMe }