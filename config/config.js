const upload = {
    MAX_FILE_SIZE_IN_BYTE: 15000000,
    ALLOWED_FILE_TYPES: /jpeg|jpg|png/
  }
  
  module.exports = upload
  