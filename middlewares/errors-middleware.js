'use strict'

/*  Error middleware
 * ----------------- */

// Load local deps
//const logger = require('../logger')

// 404
const errorHandler = (err, req, res, next) => {
  if (err.status === 404) {
    res.apiNotFound(err)
  } else {
    next()
  }
}


// Ajax error - return 500
const clientErrorHandler = (err, req, res, next) => {
  if (req.xhr) {
    res.status(500).send({ error: 'Something blew up!' })
  } else {
    next(err)
  }
}


// Bad CSRF token
const csrfTokenError = function (err, req, res, next) {
  if (err.code !== 'EBADCSRFTOKEN') {
    //logger.err(err)
    next(err)
  } else {
    res.status(403).send('Session has expired or form tampered with')
  }
}

module.exports = { errorHandler, clientErrorHandler, csrfTokenError }
