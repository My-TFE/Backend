/* eslint-disable global-require, import/no-dynamic-require */

'use strict'


const { i18n } = require('../config')
const middlewares = require('../middlewares')

/**
 * Application routes
 */

const API_VERSION = 'v1'
const API_PATH = `/api/${API_VERSION}`

// Setup Route Bindings
module.exports = app => {

  app.all('*', middlewares.api.initAPI)

  app.use(`${API_PATH}/auth`, require('./authentication').routes.routes)
  app.use(`${API_PATH}/customers`, middlewares.jwt.jwtMiddleware, require('./customers').routes.routes)
  app.use(`${API_PATH}/coach`, middlewares.jwt.jwtMiddleware, require('./coach').routes.routes)
  app.use(`${API_PATH}/sessions`, middlewares.jwt.jwtMiddleware, require('./sessions').routes.routes)
  app.use(`${API_PATH}/exercises`, middlewares.jwt.jwtMiddleware, require('./exercises').routes.routes)

  app.get('*', (req, res) => {
    res.apiNotFound(new Error(i18n.__('Invalid route')))
  })
}

