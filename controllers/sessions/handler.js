'use strict'

/**
 * Session routes
 */
const logger = require('../../logger')
const { i18n } = require('../../config')
const {getSessions, getSession, deleteSession, updateSession} = require('./queries')
const Session = require('../../models/Session')


/**
 * @desc
 *
 * @param {e.Request} req
 * @param {IApiResponse} res
 *
 * @api public
 */
const getList = async (req, res) => {
  try {
    const sessions = await getSessions({customer: req.params.id})
    res.apiResponse({
      success: true,
      status: 200,
      data: sessions
    })
  } catch (error) {
    logger.error(error)
    res.apiNotFound(new Error(i18n.__('No sessions found')))
  }
}

const createItem = async (req, res) => {
  const nSession = new Session(req.body)
  try {
    await nSession.save()
    try {
      const cSession = await getSession(nSession._id.toString(), true)
      res.apiResponse({
        success: true,
        status: 200,
        data: cSession,
        message: i18n.__('Session created with success')
      })
    } catch (error) {
      logger.error(`Failed to get new created Session (${nSession._id})`, error)
      res.apiResponse({
        success: false,
        status: 200,
        message: i18n.__('Session creation failed')
      })
    }
  } catch (error) {
    logger.error(error)
    res.apiResponse({
      success: false,
      status: 200,
      message: i18n.__('Session creation failed')
    })
  }
}

const deleteItem = async (req, res) => {
  const sessionId = req.params.id
  try {
    const session = await deleteSession(sessionId)
    res.apiResponse({
      success: true,
      status: 200,
      data: session
    })
  }catch (error) {
    logger.error(error)
    res.apiNotFound(new Error(i18n.__('No session delete')))
  }
}

const updateItem = async (req, res) => {
  const sessionId = req.params.id
  try {
    const uSession = await updateSession(sessionId, req.body)
    if (uSession) {
      res.apiResponse({
        success: true,
        status: 200,
        data: uSession,
        message: i18n.__('Session updated with success')
      })
    } else {
      res.apiResponse({
        success: false,
        status: 200,
        message: i18n.__('No Session updated')
      })
    }
  } catch (error) {
    logger.error(error)
    res.apiResponse({
      success: false,
      status: 200,
      message: i18n.__(error.message)
    })
  }
}

module.exports = { getList, createItem, deleteItem, updateItem }
