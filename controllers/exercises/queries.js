'use strict'

/**
 * Exercises queries
 */

const Exercise = require('../../models/Exercise')

const getExercise = (ExerciseId) => {
  return Exercise.findById(ExerciseId)
}

const getExercises = (options) => {
  return Exercise.find(options)
}

const updateExercise = async (id, exerciseData) => {
  return Exercise.findOneAndUpdate({_id: id}, exerciseData, {new: true})
}

const deleteExercise = async (exerciseId) => {
  return Exercise.findByIdAndRemove(exerciseId)
}

const deleteExerciseSession = (exerciseId) => {
  return Exercise.findByIdAndRemove(exerciseId)
}



module.exports = { getExercises, getExercise, getExercise, updateExercise, deleteExercise, deleteExerciseSession }
