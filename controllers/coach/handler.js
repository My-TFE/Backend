'use strict'

/**
 * coach routes
 */
const validator = require('validator')
const logger = require('../../logger')
const { i18n } = require('../../config')
const {getCoach, getCustomersByCoach} = require('./queries')
const Coach = require('../../models/Coach')





const createItem = async (req, res) => {
  const nCoach = new Coach(req.body)
  try {
    await nCoach.save()
    try {
      const cCoach = await getCoach(nCoach._id.toString(), true)
      res.apiResponse({
        success: true,
        status: 200,
        data: cCoach,
        message: i18n.__('Coach created with success')
      })
    } catch (error) {
      logger.error(`Failed to get new created coach (${nCoach._id})`, error)
      res.apiResponse({
        success: false,
        status: 200,
        message: i18n.__('Coach creation failed')
      })
    }
  } catch (error) {
    logger.error(error)
    res.apiResponse({
      success: false,
      status: 200,
      message: i18n.__('Coach creation failed')
    })
  }
} 

const getCoachCustomers = async (req, res) => {
  try {
    const customers = await getCustomersByCoach(req.params.id)
    res.apiResponse({
      success: true,
      status: 200,
      data: customers
    })
  } catch (error) {
    logger.error(error)
    res.apiNotFound(new Error(i18n.__('No customers found')))
  }

}





module.exports = {createItem, getCoachCustomers}
