/* eslint-disable global-require, import/no-dynamic-require */

'use strict'

/*
 * Session
 * ---- */

const mongoose = require('mongoose')
const conn = require('../connections/index').mongo
const { ObjectId } = mongoose.Schema.Types


/* Session Schema
 * ----------- */

const SessionSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  exercises: {
    type: [{
      type: ObjectId,
      ref: 'Exercise',
      index: true,
    }]
  },
  customer: {
    type: ObjectId,
    ref: 'Customer',
    index: true,
    required: true,
  }
})


module.exports = conn.model('Session', SessionSchema)
