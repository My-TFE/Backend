const multer = require('multer')
const path = require('path')
const {i18n} = require('../config')
const conf = require('../config/config')
const logger = require('../logger')

const uploadMulter = multer({
  storage: multer.memoryStorage({})
}).any()

const ERRORS = {
  NO_FILES: 'You must at least provide one file',
  FILE_TOO_LARGE: 'File is too large',
  NOT_ALLOWED_FILE_EXTENSION: 'Not allowed file extension'
}

const fileExtensionHandler = (req, res, next) => {
  if (req.files && req.files.length) {
    req.files.map(file => {
      const mimetype = (conf.ALLOWED_FILE_TYPES).test(file.mimetype)
      const extname = (conf.ALLOWED_FILE_TYPES).test(path.extname(file.originalname).toLowerCase())
      
      if (file.size > conf.MAX_FILE_SIZE_IN_BYTE) {
        logger.error(ERRORS.FILE_TOO_LARGE)
        res.apiResponse({
          success: false,
          status: 200,
          message: i18n.__(ERRORS.FILE_TOO_LARGE)
        })
      } else if (!(mimetype && extname)) {
        logger.error(ERRORS.NOT_ALLOWED_FILE_EXTENSION)
        res.apiResponse({
          success: false,
          status: 200,
          message: i18n.__(ERRORS.NOT_ALLOWED_FILE_EXTENSION)
        })
      } else {
        next()
      }
    })
  } else {
    logger.error(ERRORS.NO_FILES)
    res.apiResponse({
      success: false,
      status: 200,
      message: i18n.__(ERRORS.NO_FILES)
    })
  }
}

module.exports = { fileExtensionHandler, uploadMulter }