'use strict'

/**
 * Json Web Token middleware
 */

const expressJwt = require('express-jwt')
const jwt = require('jsonwebtoken')

/**
 * @desc Validate token and add user to req
 */
const jwtMiddleware = expressJwt({
  secret: Buffer.from(process.env.JWT_SUPER_SECRET, 'base64')
})

/**
 * @desc Check if JWT token exists within the header of the current request
 */
const jwtHeaderCheck = (err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    res.apiUnauthorized(err, 'No authorization token was found')
  }
  next()
}

/**
 * @desc Revoke token for the given id
 */
const revokeToken = tokenId => expressJwt({
  secret: process.env.JWT_SUPER_SECRET,
  isRevoked: (req, payload, cb) => {
    cb(undefined, payload.jti && payload.jti === tokenId)
  }
})


module.exports = { jwtMiddleware, jwtHeaderCheck, revokeToken }
