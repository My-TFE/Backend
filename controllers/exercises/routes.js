'use strict'

/**
 * Users Routes
 */

const routes = require('express').Router()
const { getList, createItem, updateItem, deleteItem, getItem } = require('./handler')


routes.get('/all/:id', getList)
routes.post('/', createItem) 
routes.put('/:id', updateItem)
routes.delete('/:id', deleteItem)
routes.get('/:id', getItem)

module.exports = { routes }
