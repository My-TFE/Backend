'use strict'

/**
 * Sessions Routes
 */

const routes = require('express').Router()
const { getList, createItem, deleteItem, updateItem } = require('./handler')

routes.get('/:id', getList)
routes.post('/', createItem)
routes.delete('/:id', deleteItem)
routes.put('/:id', updateItem)


module.exports = { routes }
