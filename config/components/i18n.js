'use strict'

/* i18n configuration
 * ------------------ */

// Load deps
const i18n = require('i18n')

const DEBUG = process.env.NODE_ENV === 'development'
const langs = process.env.LANGUAGES || 'en'

i18n.configure({
  locales: langs.split(','),
  directory: `${__dirname}/../../locales`,
  defaultLocale: 'en',
  cookie: 'locales',
  updateFiles: DEBUG
})


module.exports = i18n
