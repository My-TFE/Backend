'use strict'

/**
 * Coachs queries
 */

const Coach = require('../../models/Coach')
const Customer = require('../../models/Customer')

/**
 * @desc Get all required data about the given Customer (Customer data, ...)
 *
 * @param {ObjectId} CoachId
 * 
 *
 * @api public
 */
const getCoach = (coachId) => {
  return Coach.findById(coachId)   
}

const getCustomersByCoach = (coachId) => {
  return Customer.find({coach: coachId}).sort({ firstName: 1 })
}




module.exports = { getCoach, getCustomersByCoach }
