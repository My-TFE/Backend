/* eslint-disable global-require, import/no-dynamic-require */

'use strict'

/*
 * Coach
 * ---- */

const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcrypt')
const conn = require('../connections/index').mongo



/* Coach Schema
 * ----------- */

const CoachSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },

  email: {
    type: String,
    required: true,
    index: { unique: true }
  },

  password: {
    type: String,
    required: true
  }

})


CoachSchema.pre('save', function (next) {
  if (!this.isModified('password')) {
    return next()
  }
  bcrypt.hash(this.password, 10, (err, hash) => {
    if (err) {
      return next(err)
    }
    this.password = hash
    next()
  })
})

/* Validations
 * ----------- */

const emailValidator = function (email) {
  return validator.isEmail(email)
}

/* authenticate password
*----------------------- */

CoachSchema.methods.authenticate = function (password) {
  return bcrypt.compareSync(password, this.password)
}

CoachSchema
  .path('email')
  .validate(emailValidator, '{VALUE} is not a valid email')


/* Pre hook for save method
 * ------------------------ */

const emailExistsValidator = function (email, cb) {
  conn.model('Coach', CoachSchema).count({ email }, (err, count) => {
    if (err) {
      cb(err)
    } else if (count > 0) {
      cb(new Error(`${email} already exists`))
    } else {
      cb()
    }
  })
}

CoachSchema.pre('save', function (next) {
  if (this.isNew || this.isModified('email')) {
    emailExistsValidator(this.email, (err) => {
      if (err) {
        next(err)
      } else if (this.isNew) {
        // TODO: Add some logic here if necessary
        next()
      } else {
        next()
      }
    })
  } else {
    next()
  }
})


module.exports = conn.model('Coach', CoachSchema)
