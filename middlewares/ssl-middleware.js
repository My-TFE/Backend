'use strict'

/* SSL middleware
 * -------------- */

const sslEndpoint = function (req, res, next) {
  if (req.headers['x-forwarded-proto'] === 'https') {
    res.redirect(`https://${req.headers.host}${req.path}`)
  } else {
    next()
  }
}


module.exports = sslEndpoint
