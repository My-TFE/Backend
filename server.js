'use strict'

// Load deps
const http = require('http')
const app = require('./app')
const logger = require('./logger')

/* Server creation
 * --------------- */

// Define app port
const appPort = process.env.PORT || 5000
app.set('port', appPort)

// Create Web server
http
  .createServer(app)
  .listen(
    appPort,
    () => logger.info(`Node app running at localhost:${appPort}`)
  )
