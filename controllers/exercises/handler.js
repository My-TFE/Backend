'use strict'

/**
 * Exercise routes
 */
const logger = require('../../logger')
const { i18n } = require('../../config')
const { getExercises, getExercise, updateExercise, deleteExercise } = require('./queries')
const Exercise = require('../../models/Exercise')
const { addExercise } = require('../sessions/queries')


/**
 * @desc
 *
 * @param {e.Request} req
 * @param {IApiResponse} res
 *
 * @api public
 */
const getList = async (req, res) => {
  try {
    const Exercises = await getExercises({ session: req.params.id })
    res.apiResponse({
      success: true,
      status: 200,
      data: Exercises
    })
  } catch (error) {
    logger.error(error)
    res.apiNotFound(new Error(i18n.__('No Exercises found')))
  }
}

const createItem = async (req, res) => {
  const nExercise = new Exercise(req.body)
  try {
    await nExercise.save()
    try {
      const cExercise = await getExercise(nExercise._id.toString(), true)
      try {
        await addExercise(cExercise)
      } catch (error) {
        logger.error(`Failed to add Exercise (${cnExercise._id}) in Session Model`, error)
        res.apiResponse({
          success: false,
          status: 200,
          message: i18n.__('Session add Exercise failed')
        })
      }
      res.apiResponse({
        success: true,
        status: 200,
        data: cExercise,
        message: i18n.__('Exercise created with success')
      })
    } catch (error) {
      logger.error(`Failed to get new created Exercise (${nExercise._id})`, error)
      res.apiResponse({
        success: false,
        status: 200,
        message: i18n.__('Exercise creation failed')
      })
    }
  } catch (error) {
    logger.error(error)
    res.apiResponse({
      success: false,
      status: 200,
      message: i18n.__('Exercise creation failed')
    })
  }
}

const updateItem = async (req, res) => {
  const exerciseId = req.params.id
  try {
    const uExercise = await updateExercise(exerciseId, req.body)
    if (uExercise) {
      res.apiResponse({
        success: true,
        status: 200,
        data: uExercise,
        message: i18n.__('Exercise updated with success')
      })
    } else {
      res.apiResponse({
        success: false,
        status: 200,
        message: i18n.__('No Exercise updated')
      })
    }
  } catch (error) {
    logger.error(error)
    res.apiResponse({
      success: false,
      status: 200,
      message: i18n.__(error.message)
    })
  }
}

const deleteItem = async (req, res) => {
  const exerciseId = req.params.id
  try {
    const exercise = await deleteExercise(exerciseId)
    res.apiResponse({
      success: true,
      status: 200,
      data: exercise
    })
  }catch (error) {
    logger.error(error)
    res.apiNotFound(new Error(i18n.__('No exercise delete')))
  }
}

const getItem = async (req, res) => {
  console.log('exercise seul')
  const exerciseId = req.params.id
  try {
    const exercise = await getExercise(exerciseId)
    res.apiResponse({
      success: true,
      status: 200,
      data: exercise
    })
  }catch (error) {
    logger.error(error)
    res.apiNotFound(new Error(i18n.__('No exercise found')))
  }
}

module.exports = { getList, createItem, updateItem, deleteItem, getItem }
