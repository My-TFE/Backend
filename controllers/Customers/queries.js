'use strict'

/**
 * Customers queries
 */

const Customer = require('../../models/Customer')
const Session = require('../sessions/queries')

/**
 * @desc Get all required data about the given Customer (Customer data, ...)
 *
 * @param {ObjectId} CustomerId
 * 
 *
 * @api public
 */
const getCustomer = (customerId) => {
  return Customer.findById(customerId)
}

const getCustomerByEmail = (email) => {
  return Customer.findOne({ email })
}

const updateCustomer = (id, customerData) => {
  return Customer.findOneAndUpdate({ _id: id }, customerData, { new: true })
}

const getCustomers = (options) => {
  return Customer.find(options)

}

const deleteCustomer = (customerId) => {
  return Session.deleteSessionsCustomer({ customer: customerId })
    .then(_ => Customer.findByIdAndRemove(customerId))
}




module.exports = { getCustomer, getCustomers, getCustomerByEmail, updateCustomer, deleteCustomer }
