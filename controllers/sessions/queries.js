'use strict'

/**
 * Sessions queries
 */

const Session = require('../../models/Session')
const Exercise = require('../../models/Exercise')


const getSession = (sessionId) => {
  return Session.findById(sessionId)
}

const getSessions = (options) => {
  return Session.find(options)
    .sort({ createdAt: -1 })
    .populate('exercises')

}

const updateSession = async (id, sessionData) => {
  return Session.findOneAndUpdate({ _id: id }, sessionData, { new: true })
}

const addExercise = (exercise) => {
  return Session.update({ _id: exercise.session }, { $push: { exercises: exercise } })
}

const deleteSession = (sessionId) => {
  return Session.findByIdAndRemove(sessionId)
    .then(session => {
      session.exercises.forEach(ex => {
        Exercise.findByIdAndRemove(ex).exec()
      })
      return session
    })
}

const deleteSessionsCustomer = (option) => {
  return Session.find(option)
    .then(sessions => sessions.forEach(session => {
      session.exercises.forEach(ex => {
        Exercise.findOneAndRemove(ex).exec()
      })
    }))
    .then(_ => Session.remove(option))
}


module.exports = { getSessions, getSession, deleteSession, updateSession, addExercise, deleteSessionsCustomer }
