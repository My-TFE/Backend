/* eslint-disable global-require, import/no-dynamic-require */

'use strict'

// Load ENV vars
const dotEnvOption = {
  silent: true,
  path: `env/development.env`
}

if (process.env.NODE_ENV === 'production') {
  dotEnvOption.path = 'env/production.env'
} else if (process.env.NODE_ENV === 'staging') {
  dotEnvOption.path = 'env/staging.env'
} else if (process.env.NODE_ENV === 'acceptance') {
  dotEnvOption.path = 'env/acceptance.env'
}

require('dotenv').config(dotEnvOption)

// Load deps
const path = require('path')
const express = require('express')
const compress = require('compression')
const helmet = require('helmet')
const cors = require('cors')
const csrf = require('csurf')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const middlewares = require('./middlewares')
const { i18n } = require('./config')
const logger = require('./logger')

const DEBUG = process.env.NODE_ENV === 'development'


/* Create Express instance
 * ----------------------- */
const app = express()


/* Assets setup
 * ------------ */
app.use(express.static(path.join(__dirname, 'public')))


/* Setup default middlewares
 * ------------------------- */
app.use(compress())
app.use(bodyParser.json({ limit: '1mb' }))
app.use(bodyParser.urlencoded({ limit: '1mb', extended: true }))


/**
 * Logging
 */
morgan.token('current_user', (req) => (req.user ? req.user.name : 'anonymous'))

app.use(morgan(
  'user=:current_user - method=:method -  url=:url -'
  + ' status=:status - response-time=:response-time - content-length='
  + ':res[content-length]'
  , { stream: logger.stream }
))


/* Configure i18n
 * -------------- */
app.use(i18n.init)


/* Proxy settings
 * -------------- */
if (!DEBUG) {
  app.set('trust proxy', 1) // Trust first proxy
}

/* Setup security middlewares
 * -------------------------- */
// app.use(csrf({ cookie: true }))
app.use(helmet.hsts({ maxAge: 10886400000, includeSubdomains: true }))
app.use(cors({ origin: '*' }))


/* Setup production middlewares
 * ---------------------------- */
if (!DEBUG && process.env.IS_SECURE === 'true') {
  app.use(middlewares.ssl)
}


/* Controllers
 * ----------- */
require('./controllers')(app)

module.exports = app
