'use strict'

/**
 * Coatch Routes
 */

const routes = require('express').Router()
const { createItem, getCoachCustomers} = require('./handler')
const middlewares = require('../../middlewares')


routes.post('/', createItem) 
routes.get('/:id/customers',middlewares.jwt.jwtMiddleware, getCoachCustomers)


module.exports = { routes }
