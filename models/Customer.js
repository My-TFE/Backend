/* eslint-disable global-require, import/no-dynamic-require */

'use strict'

/*
 * Customer
 * ---- */

const mongoose = require('mongoose')
const conn = require('../connections/index').mongo
const { ObjectId } = mongoose.Schema.Types



/* Customer Schema
 * ----------- */

const CustomerSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    index: { unique: true }
  },
  phoneNumber: {
    type: String
  },
  birthday: {
    type: Date
  },
  bodyMeasure: {
    type: [{
      height: { type: Number, default: 0 },
      weight: { type: Number, default: 0 },
      shoulder: { type: Number, default: 0 },
      pectorals: { type: Number, default: 0 },
      arm: { type: Number, default: 0 },
      belly: { type: Number, default: 0 },
      hips: { type: Number, default: 0 },
      thigh: { type: Number, default: 0 },
      calf: { type: Number, default: 0 },
      measuredAt: { type: Date, default: Date.now }
    }]
  },
  coach: {
    type: ObjectId,
    ref: 'Coach',
    index: true,
    required: true,
  }
})


/* Validations
 * ----------- */


const emailExistsValidator = function (email, cb) {
  conn.model('Customer', CustomerSchema).count({ email }, (err, count) => {
    if (err) {
      cb(err)
    } else if (count > 0) {
      cb(new Error(`${email} already exists`))
    } else {
      cb()
    }
  })
}

CustomerSchema.pre('save', function (next) {
  if (this.isNew || this.isModified('email')) {
    emailExistsValidator(this.email, (err) => {
      if (err) {
        next(err)
      } else if (this.isNew) {
        // TODO: Add some logic here if necessary
        next()
      } else {
        next()
      }
    })
  } else {
    next()
  }
})


module.exports = conn.model('Customer', CustomerSchema)
