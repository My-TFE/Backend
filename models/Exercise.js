/* eslint-disable global-require, import/no-dynamic-require */

'use strict'

/*
 * Exercise
 * ---- */

const mongoose = require('mongoose')
const conn = require('../connections/index').mongo
const { ObjectId } = mongoose.Schema.Types


/* Exercise Schema
 * ----------- */

const ExerciseSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  recoveryTime: Number,
  createdAt: {
    type: Date,
    default: Date.now
  },
  sets: {
    type: [{
      weight: Number,
      nbrRepetition: Number
    }]
  },
  session: {
    type: ObjectId,
    ref: 'Session',
    index: true,
    required: true,
  }

})


module.exports = conn.model('Exercise', ExerciseSchema)
