'use strict'

/**
 * Customer routes
 */

const logger = require('../../logger')
const { i18n } = require('../../config')
const { getCustomer, getCustomers, updateCustomer, deleteCustomer } = require('./queries')
const Customer = require('../../models/Customer')


/**
 * @desc Get the list of all Customers
 *
 * @param {e.Request} req
 * @param {IApiResponse} res
 *
 * @api public
 */
const getList = async (req, res) => {
  try {
    const customers = await getCustomers({})
    res.apiResponse({
      success: true,
      status: 200,
      data: customers
    })
  } catch (error) {
    logger.error(error)
    res.apiNotFound(new Error(i18n.__('No Customers found')))
  }
}

const updateItem = async (req, res) => {
  const customerId = req.params.id
  try {
    const uCustomer = await updateCustomer(customerId, req.body)
    if (uCustomer) {
      res.apiResponse({
        success: true,
        status: 200,
        data: uCustomer,
        message: i18n.__('Customer updated with success')
      })
    } else {
      res.apiResponse({
        success: false,
        status: 200,
        message: i18n.__('No customer updated')
      })
    }
  } catch (error) {
    logger.error(error)
    res.apiResponse({
      success: false,
      status: 200,
      message: i18n.__(error.message)
    })
  }
}


 

/**
 * @desc Get customer for the given id
 *
 * @param {e.Request} req
 * @param {IApiResponse} res
 *
 *  @api public
 */

const getItem = async (req, res) => {
  const customerId = req.params.id
  try {
    const customer = await getCustomer(customerId)
    res.apiResponse({
      success: true,
      status: 200,
      data: customer
    })
  }catch (error) {
    logger.error(error)
    res.apiNotFound(new Error(i18n.__('No customer found')))
  }
}


const createItem = async (req, res) => {
  const nCustomer = new Customer(req.body)
  try {
    await nCustomer.save()
    try {
      const cCustomer = await getCustomer(nCustomer._id.toString(), true)
      res.apiResponse({
        success: true,
        status: 200,
        data: cCustomer,
        message: i18n.__('Customer created with success')
      })
    } catch (error) {
      logger.error(`Failed to get new created customer (${nCustomer._id})`, error)
      res.apiResponse({
        success: false,
        status: 200,
        message: i18n.__('Customer creation failed')
      })
    }
  } catch (error) {
    logger.error(error)
    res.apiResponse({
      success: false,
      status: 200,
      message: i18n.__('Customer creation failed')
    })
  }
} 

const deleteItem = async (req, res) => {
  const customerId = req.params.id
  try {
    const customer = await deleteCustomer(customerId)
    res.apiResponse({
      success: true,
      status: 200,
      data: customer
    })
  }catch (error) {
    logger.error(error)
    res.apiNotFound(new Error(i18n.__('No customer delete')))
  }
}



module.exports = { getList, createItem, updateItem, getItem, deleteItem} //, getItem,  }
