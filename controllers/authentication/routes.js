'use strict'

/**
 * Authentication Routes
 */

const routes = require('express').Router()
const { login, getMe } = require('./handler')
const middlewares = require('../../middlewares')

routes.post('/login', login)
routes.get('/me', middlewares.jwt.jwtMiddleware, getMe)

module.exports = { routes }
