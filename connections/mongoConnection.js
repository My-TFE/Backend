'use strict'

// Load ENV vars
const DEBUG = process.env.NODE_ENV === 'development'

// Load deps
const mongoose = require('mongoose')
const uriUtil = require('mongodb-uri')
const logger = require('../logger')

// Create a new connection
mongoose.Promise = global.Promise
// mongoose.set('debug', DEBUG)

const dbURI = uriUtil.formatMongoose(process.env.MONGO_URI)
const options = {
  autoIndex: DEBUG,
  autoReconnect: true
}
const conn = mongoose.createConnection(dbURI, options)

conn.on('open', () => logger.info('DB connection open'))
conn.on('error', (err) => logger.error(`DB connection error : ${err.message}`))


module.exports = conn
